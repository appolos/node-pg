-- create database library;
-- List databases
\l

-- connect or use the database
-- \c library

-- create table books
create table journals(
    title text,
    author text,
    theme text
);

-- list tables
\dt

-- describe table
\d journals

-- insert data
insert into journals values 
    ('NG', 'Foundation', 'Isaac Asimov'),
    ('NG', 'Digital Fortress', 'Dan Brown'),
    ('NG', 'World War Z', 'Max Brooks');

-- list data
select * from journals;
