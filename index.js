const express = require("express");
const BookRouter = require("./routes/booksRoutes");
const AuthRouter = require("./routes/authRoutes");
const app = express();

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Add route code Here
app.get("/", (req, res) => {
  res.send("API is working");
});
app.use("/api/auth", AuthRouter);
app.use("/api/books", BookRouter);
app.listen(port, () => {
  console.log(`We are live at 127.0.0.1:${port}`);
});
