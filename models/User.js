const bcrypt = require("bcrypt");
const db = require("../db");
const uuid = require("uuid/v4");

module.exports = {
  async create(email, password) {
    try {
      const text = `INSERT INTO users (id, email, password)
      VALUES($1, $2, $3)
      returning *`;
      const { rows } = await db.query(text, [email, password]);
      const [user] = rows;

      return user;
    } catch (error) {
      if (error.constraint === "users_email_key") {
        return null;
      }

      throw error;
    }
  },
  async find(email) {
    const text = `SELECT * FROM users WHERE email=$1 LIMIT 1;`;
    const { rows } = await db.query(text, email);
    return rows[0];
  },
  async update(email) {
    const text = `SELECT * FROM users WHERE email=$1 LIMIT 1;`;
    const { rows } = await db.query(text, email);
    return rows[0];
  }
};
