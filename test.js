require("dotenv").config();
const express = require("express");
const sendUploadToGCS = require("./middleware/uploadImages");

const Multer = require("multer");
const upload = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 5 * 1024 * 1024, // no larger than 5mb
  },
});

const app = express();

const PORT = process.env.PORT;

app.post("/", upload.single("image"), sendUploadToGCS, (req, res) => {
  if (req.file && req.file.cloudStoragePublicUrl) {
    return res.json({
      msg: "Image uploaded",
      url: req.file.cloudStoragePublicUrl,
    });
  }
  res.json({
    msg: "Image did not uploaded",
  });
});

app.listen(PORT, () => {
  console.log("Server starting");
});
