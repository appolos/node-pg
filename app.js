require('dotenv').config()
const express = require('express');
var admin = require("firebase-admin");

var serviceAccount = require("./todoist-app-52f31-firebase-adminsdk-onao4-92cf45bfff.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  storageBucket: "todoist-app-52f31.appspot.com"
});

var bucket = admin.storage().bucket();

const Multer = require('multer');
const upload = Multer({
  storage: Multer.MemoryStorage,
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb
  }
});


const app = express();

const PORT = process.env.PORT;

app.post('/', upload.single('picture'),(req, res) => {
    //console.log(req.file)
    const a = bucket.file(req.file.originalname)
    bucket.upload(a.name,{})
        .then(data=>console.log(data))
    console.log(a)
    res.json({ msg: "Test" })
})

app.listen(PORT, () => {
    console.log('Server starting')
    //console.log(bucket)

})