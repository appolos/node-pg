// const { Pool } = require('pg')
// const pool = new Pool()
// module.exports = {
//   query: (text, params, callback) => {
//     return pool.query(text, params, callback)
//   },
// }
require('dotenv').config();
const pg = require('pg');

const config = {
  user: process.env.DB_USER, 
  database: process.env.DB_DATABASE,
  password: process.env.DB_PASSWORD,
  port: process.env.DB_PORT,
  host: process.env.DB_HOST,
  max: 10, // max number of clients in the pool
  idleTimeoutMillis: 30000,
};

const pool = new pg.Pool(config);

pool.on('connect', () => {
  console.log('connected to the Database');
});

//module.exports.pool = pool;
module.exports = {
  query: (text, params) => pool.query(text, params),
}