FROM node:carbon

    
WORKDIR /usr/src/app
    
COPY package*.json ./
RUN apt-get update
RUN apt-get install sudo

RUN adduser --disabled-password --gecos '' docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER docker

RUN sudo apt-get update && sudo apt-get install -y build-essential && sudo apt-get install -y python && npm install

RUN sudo apt-get update 
    
RUN sudo npm install
RUN sudo npm install -g pm2
    
COPY . ./
    
EXPOSE 3000
EXPOSE 9200
    
CMD npm run start