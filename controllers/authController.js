const User = require("../models/User");
const bcrypt = require("bcrypt");
const { body, validationResult } = require("express-validator");

module.exports = {
  async login(req, res) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      res.send("TEST LOGN");
    } catch (error) {}
  },
  async register(req, res) {
    const { firstName, lastName, email, password } = req.body;
    try {
      const hashedPassword = await bcrypt.hash(password, 10);

      const data = await User.create(
        firstName,
        lastName,
        email,
        hashedPassword
      );

      res.status(200).send({ data });
    } catch (error) {
      res.status(400).send({ message: "Something went wrong" });
    }
  },
  validate(method) {
    switch (method) {
      case "createUser": {
        return [
          body("userName", "userName doesn't exists").exists(),
          body("email", "Invalid email")
            .exists()
            .isEmail(),
          body("phone")
            .optional()
            .isInt(),
          body("status")
            .optional()
            .isIn(["enabled", "disabled"])
        ];
      }
      case "loginuser": {
        return [
          body("userName", "This user name doesn't exists").exists(),
          body("email", "Invalid email")
            .isEmail()
        ];
      }
    }
  }
};
