const Book = require("../models/Book");
const bcrypt = require("bcrypt");
const { body } = require("express-validator/check");

module.exports = {
  async getOne(email, password) {
    try {
        console.log('Get one Book')
    } catch (error) {}
  },
  async getAll(req, res) {
    const { firstName, lastName, email, password } = req.body;
    try {
      const hashedPassword = await bcrypt.hash(password, 10);

      const data = await User.create(
        firstName,
        lastName,
        email,
        hashedPassword
      );

      res.status(200).send({ data });
    } catch (error) {
      res.status(400).send({ message: "Something went wrong" });
    }
  },
  async update(req, res) {
    const { firstName, lastName, email, password } = req.body;
    try {
      const hashedPassword = await bcrypt.hash(password, 10);

      const data = await User.create(
        firstName,
        lastName,
        email,
        hashedPassword
      );

      res.status(200).send({ data });
    } catch (error) {
      res.status(400).send({ message: "Something went wrong" });
    }
  },
  async delete(req, res) {
    const { firstName, lastName, email, password } = req.body;
    try {
      const hashedPassword = await bcrypt.hash(password, 10);

      const data = await User.create(
        firstName,
        lastName,
        email,
        hashedPassword
      );

      res.status(200).send({ data });
    } catch (error) {
      res.status(400).send({ message: "Something went wrong" });
    }
  },
  validate(method) {
    switch (method) {
      case "createUser": {
        return [
          body("userName", "userName doesn't exists").exists(),
          body("email", "Invalid email")
            .exists()
            .isEmail(),
          body("phone")
            .optional()
            .isInt(),
          body("status")
            .optional()
            .isIn(["enabled", "disabled"])
        ];
      }
      case "loginuser": {
        return [
          body("userName", "userName doesn't exists").exists(),
          body("email", "Invalid email")
            .exists()
            .isEmail(),
          body("phone")
            .optional()
            .isInt(),
          body("status")
            .optional()
            .isIn(["enabled", "disabled"])
        ];
      }
    }
  }
};
