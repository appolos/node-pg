module.exports = function(filename) {
  return `https://storage.googleapis.com/${process.env.BUCKET_NAME}/${filename}`;
};
