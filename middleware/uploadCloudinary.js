require('dotenv').config()
const express = require('express');

const cloudinary = require('cloudinary');
const cloudinaryStorage = require('multer-storage-cloudinary');
const multer = require('multer');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});

const storage = cloudinaryStorage({
    cloudinary,
    folder: 'jomwedding',
    allowedFormats: ['jpg', 'png'],
    transformation: [{ width: 500, height: 500, crop: 'limit' }]
});
const parser = multer({ storage });

const app = express();

const PORT = process.env.PORT;
function uploadImage(req, res, next) {
    console.log(req.file);
    const image = {};
    image.url = req.file.url;
    image.id = req.file.public_id;
    console.log(image)
    res.send({image})
}
app.get('/', (req, res) => res.json({ msg: "Test" }))
app.post('/image', parser.single('image'), uploadImage);
