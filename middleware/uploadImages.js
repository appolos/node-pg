const getPublicUrl = require("../helpers/createGSCUrl");
const { Storage } = require("@google-cloud/storage");
const storage = new Storage({
  projectId: process.env.PROGECT_ID,
  keyFilename: "./todoist-app-52f31-firebase-adminsdk-onao4-92cf45bfff.json",
});
const bucket = storage.bucket(process.env.BUCKET_NAME);
module.exports = function sendUploadToGCS(req, res, next) {
  const { file } = req;
  if (!file) {
    return next();
  }

  const gcsFilename = Date.now() + file.originalname;
  const file = bucket.file(gcsFilename);

  const stream = file.createWriteStream({
    metadata: {
      contentType: file.mimetype,
    },
    resumable: false,
  });

  stream.on("error", err => {
    req.file.cloudStorageError = err;
    next(err);
  });

  stream.on("finish", () => {
    req.file.cloudStorageObject = gcsname;
    file.makePublic().then(() => {
      req.file.cloudStoragePublicUrl = getPublicUrl(gcsname);
      next();
    });
  });

  stream.end(req.file.buffer);
};
