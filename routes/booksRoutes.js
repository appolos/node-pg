const express = require('express');
const router = express.Router();
const Book = require('../models/Book');

/**
 * 
 * Get All Books
 * @function
 * @param req {Object} The request.
 * @param res {Object} The response.
 * @return {undefined}
 */
router.get('/', async (req, res) => {
    try {
        const data = await Book.getAll();
        return res.status(200).send(data);
    } catch (e) {
        return res.status(400).send(e);
    }
})

/**
 * 
 * Get Book by ID
 * @function
 * @param req {Object} The request.
 * @param res {Object} The response.
 * @return {undefined}
 */
router.get('/:id', async (req, res) => {
    try {
        const data = await Book.getOne(req.params.id);
        return res.status(200).send(data);
    } catch (e) {
        return res.status(400).send(e);
    }
})

module.exports = router;