const express = require("express");
const router = express.Router();
const authController = require("../controllers/authController");

router.post("/login", authController.validate('loginuser'), authController.login);
router.get("/change-password", authController.login);
router.post("/register", authController.register);

module.exports = router;
